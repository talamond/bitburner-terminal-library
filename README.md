# Bitburner Terminal Wrapper Library

This library provides a `terminal` class object that exposes and wraps the
native terminal object that Bitburner uses behind the scenes.

### Usage

To get started quickly, you can use Statically to import the library directly
from this repository using:
`import { terminal } from 'https://cdn.statically.io/gl/talamond/bitburner-terminal-library/main/terminal.js'`

Whether you're importing through web or local, you then need to create a fresh
instance using `let term = new terminal()`. (This is necessary as it runs the
code to pull in the terminal object from the environment)

### Functions of terminal

`terminal.cwd` - The odd man out. This just passes through to the parent
function and returns a string of the current working directory the user is at
in the terminal.

`terminal.print` - A drop in replacement for `ns.tprint`, only difference is
that this won't clutter your terminal with a garbage prefix.

`terminal.formatPrint` - Takes inputs as formatted pairs of strings. Examples
given below.

`terminal.getInput` - Returns the contents of the terminal input field.

`terminal.setInput` - Sets the contents of the terminal input field.

`terminal.terminalExec` - Executes whatever is in input, optionally takes a
string to execute. **NOTE:** Only works while terminal is visible on screen,
will return `true` if successful so you can check.

`terminal.term` - This is the native terminal object, in case you ever need to
mess with it.

### formatPrint Quickstart

This function takes pairs of strings as input, the first in each pair is a
set of formatting options.

The available formatting options are: `b`, `i`, and `u` representing bold,
italics, and underline respectively. Anything else given is assumed to be an
html/css color code taking color names as well as hex values (differentiated
with a preceding `#`).

You can use multiple values by seperating them with a :, for instance
`"b:i:blue"` will make the text bold, italics, and blue.

While there is no current "no formatting" option, just giving simple junk
data here like `''` will effectively result in default formatting.

Example usage:

`terminal.formatPrint('b:red', "This is bold and red! ", 'blue', "And this is blue! ", '', "This text is normal. ", '#ffff00', "And this text is yellow!")`

### Internal Functions

`findProp` - This is a function copied raw from the contribution of @Omuretsu.
It's used to grab the terminal object and is the big workhorse of this library.

`setColor`, `setBold`, `setItalics`, `setUnderline` - These each wrap the given
argument in a React object setting the specified formatting. These can be used
directly with the depricated `fancyPrint` function, otherwise they're used by
the `formatPrint` function to apply the selected formats.

### Depricated Functions

The `fancyPrint` function has been superceded by `formatPrint`, but kept
currently because it doesn't break anything. It's just a wrapper for 
the `printRaw` function of the terminal object, which accepts a React object
to output to the terminal.