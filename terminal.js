/** @param {NS} ns **/

const GUIDE=
"Terminal Library:\n" +
"Provides the terminal class object containing:\n" +
"    terminal.term: a direct handle for the native terminal class\n" +
"    terminal.print(string): Prints natively to the terminal without the added script name.\n" +
"    terminal.cwd(): Returns the current terminal directory.\n" +
"    terminal.getInput(): Returns the current contents of the terminal's input field.\n" +
"    terminal.setInput(string): Sets the terminal's input field to given string.\n" +
"    terminal.terminalExec(<opt>string): Executes terminal *if visible*, optionally accepts string for command, returns true if successful.\n" +
"    terminal.formatPrint(format, text, ...): Takes format arguments and text in pairs.\n" +
"            Formats: i=italics, b=bold, u=underline, or any color name. Can be separated with\n" +
"                     ':' to use multiple.\n" +
"            Example: terminal.formatPrint('b:i:white', 'this text is bold, italics, and white', 'b', 'this text is just bold')"

// Terminal class to print to terminal with added features and without scriptname
// prefix.

// Also pulls in utility functions from the terminal object.

export class terminal {
	// Constructor is mostly to perform the exploit to bypass memory cost of document
	constructor () {
		// Grab the display terminal object
		this.term = this.findProp("terminal");
	}
	
	// Input handles change whenever you move to/from terminal, so breaking
	// them into a seperate function to easily grab them
	getInputHandles() {
		let results = {}
		
		// Grab the input field for the terminal
		results["input"] = eval("document").getElementById("terminal-input");
		// Grab the event handler for the input object
		results["eventHandler"] = Object.keys(results["input"])[1];
		
		return results;
	}
	
	// Use a wrapper for getInput to encourage setInput to be used
	getInput () {
		return this.getInputHandles()["input"].value
	}
	
	// Set input combines setting the value and triggering onChange to update
	setInput (text) {
		let handles = this.getInputHandles();
		
		handles["input"].value = text;
		
		// Perform onChange event to set some internal values
		handles["input"][handles["eventHandler"]].onChange({target:handles["input"]});
	}
	
	// Function to basically just hit enter on the terminal input
	// Added functionality to accept text as input so a single command can be used
	// Also check the results, will return false if the terminal couldn't be
	//   activated (only works when viewing terminal
	terminalExec () {
		let handles = this.getInputHandles();
		
		// Make the function multipurpose, if it has an argument, then set it
		// before executing
		if (arguments.length > 0) {
			this.setInput(arguments[0])
		}
		
		// Grab the terminal contents. If command is executed it will clear 
		// automatically.
		let clearCheck = this.getInput();
		
		// Simulate an enter press
		handles["input"][handles["eventHandler"]].onKeyDown({key:"Enter",preventDefault:()=>null});
		
		// Check if input cleared and return the result
		// Clear if necessary to avoid junk
		if (this.getInput() == clearCheck) {
			this.setInput(" ");
			return false;
		} else {
			return true;
		}
		
	}
	
	// 

	// Functioned copied from @omuretsu in Discord to grab terminal object
	// this took a lot of work because apparently the electron version likes to shuffle this object around...
	findProp(propName) {
		for (let div of eval("document").querySelectorAll("div")) {
		let propKey = Object.keys(div)[1];
		if (!propKey) continue;
		let props = div[propKey];
		if (props.children?.props && props.children.props[propName]) return props.children.props[propName];
		if (props.children instanceof Array) for (let child of props.children) if (child?.props && child.props[propName]) return child.props[propName];
	}
	}

	// Simple print function, does not provide any formatting features.
	// Essentially replicates ns.tprint without the scriptname used as a prefix
	print() {
		if (arguments.length == 0) {
			this.term.print('\n')
		} else {
			this.term.print([...arguments].join(''));
		}
	}

	// Fancy print function that takes React elements to output formatted text
	fancyPrint(text) {
		this.term.printRaw(text)
	}

	// Add DIV tag with color css easily
	setColor(text, color) {
		return React.createElement("span", {style: {color: `${color}`}}, text);
	}

	// Functions to add basic formatting easily
	setBold(text) {
		return React.createElement("b", null, text)
	}

	setUnderline(text) {
		return React.createElement("u", null, text)
	}

	setItalics(text) {
		return React.createElement("i", null, text)
	}

	// Returns the current directory on the server
	cwd() {
		return this.term.cwd();
	}


	formatPrint() {
		if (arguments.length % 2 != 0) throw("formatPrint arguments must come in pairs. Format,Text");
		let out = [];
		for (let i=0;i<arguments.length;i+=2) {
			let control = arguments[i].split(":");
			let item = arguments[i+1];
			for (let x=0; x < control.length;x++) {
				if (control[x] == 'bold' || control[x] == 'b') {
					item = this.setBold(item);
				} else if (control[x] == 'italics' || control[x] == 'i') {
					item = this.setItalics(item);
				} else if (control[x] == 'underline' || control[x] == 'u') {
					item = this.setUnderline(item);
				} else {
					item = this.setColor(item, control[x])
				}
			}
			out.push(item);
		}

		this.fancyPrint(out);
	}
}

export async function main(ns) {
	const term = new terminal();
	term.print(GUIDE);
}
